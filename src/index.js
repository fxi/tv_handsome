let x,
  y,
  h,
  w,
  d,
  f,
  p,
  cx,
  rct,
  ch,
  dt,
  sm,
  g = 1.6,
  dc = () => document,
  wd = () => window,
  el = (t) => dc().createElement(t),
  v = el('video'),
  c = el('canvas'),
  dp = wd().devicePixelRatio,
  co = dc().getElementById('out'),
  cor = () => co.getBoundingClientRect(),
  cox = co.getContext('2d'),
  m = Math,
  u = () => {
    rct = cor();
    w = m.ceil(rct.width / 22);
    v.width = w;
    h = m.ceil(v.videoHeight / (v.videoWidth / w));
    v.height = h;
    c.width = w;
    c.height = h;
    co.style.height = (rct.height * w) / h + 'px';
    rct = cor();
    co.width = rct.width * dp;
    co.height = rct.height * dp;
    f = co.width / w;
    cox.font = `${20 * dp}px Arial`;
  },
  rsl = (t) =>
    pl
      .map((e) => ({
        d: ds(e.c, t),
        u: e.u,
      }))
      .reduce((t, e, c) => (0 === c ? e : e.d < t.d ? e : t)),
  ds = (t, e) => {
    let c,
      d = 0;
    for (c = 0; c < 3; c++) {
      d += (t[c] - e[c]) * (t[c] - e[c]);
    }
    return Math.sqrt(d);
  },
  ii = async () => {
    try {
      if (d) {
        return;
      }
      wd().addEventListener('resize', u);
      sm = await navigator.mediaDevices.getUserMedia({
        video: 1,
        audio: 0,
      });
      d = 1;
      v.srcObject = sm;
      v.play();
      v.addEventListener(
        'canplay',
        () => {
          u();
          r();
        },
        false,
      );
    } catch (t) {
      alert(':(');
    }
  },
  r = () => {
    cx = c.getContext('2d');
    cx.drawImage(v, 0, 0, w, h);
    dt = cx.getImageData(0, 0, w, h).data;
    cox.clearRect(0, 0, co.width, co.height);
    for (y = 0; y < h; y++) {
      for (x = 0; x < w; x++) {
        p = 4 * x + 4 * y * w;
        ch = rsl([dt[p] * g, dt[p + 1] * g, dt[p + 2] * g]).u;
        cox.fillText(ch, x * f, (y + 1) * f);
      }
    }
    requestAnimationFrame(r);
  };
const pl = [
  {
    u: '☠️',
    c: [217, 217, 217],
  },
  {
    u: '😃',
    c: [240, 216, 74],
  },
  {
    u: '🎄',
    c: [111, 220, 81],
  },
  {
    u: '🍊',
    c: [206, 122, 44],
  },
  {
    u: '🥶',
    c: [92, 151, 226],
  },
  {
    u: '🍆',
    c: [156, 62, 246],
  },
  {
    u: '❤️',
    c: [200, 49, 40],
  },
  {
    u: '🐻',
    c: [129, 87, 56],
  },
  {
    u: '💣',
    c: [0, 0, 0],
  },
];
ii();
